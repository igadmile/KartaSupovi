import {
  createLayers,
  addLayersFirstLoad,
  addLayersAfterLoad,
  clearLayers,
  addLayerControl,
} from './layer-helpers';

const appendPre = (message) => {
  const pre = document.getElementById('content');
  const textContent = document.createTextNode(`${message}\n'`);
  pre.appendChild(textContent);
  pre.style.display = 'block';
};

const firstLoad = (gapi) => {
  gapi.client.sheets.spreadsheets.values
    .get({
      spreadsheetId: 'REPLACE_STRING_spreadsheetId',
      range: 'REPLACE_STRING_range',
    })
    .then(
      () => {
        createLayers();
        addLayersFirstLoad();
        addLayerControl();
      },
      response => appendPre(`Error: ${response.result.error.message}`),
    );
};

const afterLoad = (gapi) => {
  gapi.client.sheets.spreadsheets.values
    .get({
      spreadsheetId: 'REPLACE_STRING_spreadsheetId',
      range: 'REPLACE_STRING_range',
    })
    .then(
      () => {
        clearLayers();
        addLayersAfterLoad();
      },
      response => appendPre(`Error: ${response.result.error.message}`),
    );
};

export { firstLoad, afterLoad };
