import L from 'leaflet';

const onEachFeaturePoints = (feature, layer) => {
  const description = feature.properties.Description ? feature.properties.Description : '';
  const popupContent = `<div style='text-align: center; margin-left: auto; margin-right: auto;font-size:14px;'>
      <strong>${feature.properties.Name}</strong>
      </div>
      <div>${description}</div>`;
  layer.bindPopup(popupContent);
};

const createPointLayer = color => new L.geoJson(null, {
  onEachFeature: onEachFeaturePoints,
  style: {
    radius: 4,
    color: 'rgb(255,255,255)',
    weight: 1.5,
    fillOpacity: 1,
    fillColor: color,
  },
  pointToLayer: (feature, latlng) => L.circleMarker(latlng),
});

const createLastPointLayer = () => new L.geoJson(null, {
  onEachFeature: onEachFeaturePoints,
});

export { createPointLayer, createLastPointLayer };
