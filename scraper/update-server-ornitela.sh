#!/bin/bash
HOST=''
USER=''
PASSWD=''

# needed becouse of crontab
cd /PATH/TO/SCRAPER/DIRECTORY/

python3 ornitela-scraper.py download-location JSON-object-animals start-date username password
./kml-geojson-ornitela.sh
ftp -n -v $HOST << EOT
ascii
user $USER $PASSWD
prompt
cd DIRECTORY/ON/SERVER
mput ./*.json

bye
EOT

rm ./*.json
