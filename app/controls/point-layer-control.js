import { afterLoad } from '../main';

const pointLayerListener = (gapi) => {
  document.getElementById('show-points').onchange = () => afterLoad(gapi);
};

export default pointLayerListener;
