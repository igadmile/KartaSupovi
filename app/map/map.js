import L from 'leaflet';
import baseLayers from './map-baselayers';
import DateControl from './map-date-control';
import MainControl from './map-main-control';

const animals = ['REPLACE_STRING_animals'];

const layerCluster = {};
const pointLayers = {};
const lineLayers = {};
const lastPoint = {};
const overlays = {};

const map = L.map('map', {
  center: [44.598, 16.589],
  zoom: 7,
  renderer: L.canvas({
    tolerance: 5,
  }),
});

animals.forEach((animal) => {
  pointLayers[animal] = null;
  lineLayers[animal] = null;
  lastPoint[animal] = null;
  layerCluster[animal] = L.featureGroup();
  overlays[animal] = layerCluster[animal];
});

const layerControl = L.control.layers(baseLayers, overlays, {
  collapsed: false,
});

L.control.mainControl = opts => new MainControl(opts);
L.control.dateControl = opts => new DateControl(opts);

L.control
  .mainControl({
    position: 'bottomleft',
  })
  .addTo(map);

L.control
  .dateControl({
    position: 'bottomright',
  })
  .addTo(map);

L.control.scale().addTo(map);

baseLayers['Thunderforest-Outdoors'].addTo(map);

export {
  animals, pointLayers, lastPoint, layerControl, layerCluster, lineLayers, map,
};
