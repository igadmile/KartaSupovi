import { removeLayers } from './layer-helpers';
import { firstLoad } from './main';
import dateChangeListeners from './controls/date-control';
import refreshButtonListener from './controls/main-control';
import pointLayerListener from './controls/point-layer-control';

require('google-client-api')().then((gapi) => {
  const authorizeButton = document.getElementById('authorize-button');
  const signoutButton = document.getElementById('signout-button');
  const warning = document.getElementById('content');
  const dateControl = document.getElementById('date-control');

  // Client ID and API key from the Developer Console
  const CLIENT_ID = 'REPLACE_STRING_CLIENT_ID';
  const API_KEY = 'REPLACE_STRING_API_KEY';

  // Array of API discovery doc URLs for APIs used by the quickstart
  const DISCOVERY_DOCS = ['https://sheets.googleapis.com/$discovery/rest?version=v4'];

  // Authorization scopes required by the API; multiple scopes can be
  // included, separated by spaces.
  const SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly';

  const handleAuthClick = () => gapi.auth2.getAuthInstance().signIn();

  const handleSignoutClick = () => {
    gapi.auth2.getAuthInstance().signOut();
    removeLayers();
  };

  /**
   *  Called when the signed in status changes, to update the UI
   *  appropriately. After a sign-in, the API is called.
   */
  const updateSigninStatus = (isSignedIn) => {
    warning.style.display = 'none';
    warning.innerHTML = '';
    if (isSignedIn) {
      authorizeButton.style.display = 'none';
      signoutButton.style.display = 'block';
      dateControl.style.display = 'block';
      firstLoad(gapi);
    } else {
      authorizeButton.style.display = 'block';
      signoutButton.style.display = 'none';
      dateControl.style.display = 'none';
    }
  };

  /**
   *  Initializes the API client library and sets up sign-in state
   *  listeners.
   */
  const initClient = () => {
    gapi.client
      .init({
        apiKey: API_KEY,
        clientId: CLIENT_ID,
        discoveryDocs: DISCOVERY_DOCS,
        scope: SCOPES,
      })
      .then(() => {
        // Listen for sign-in state changes.
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

        // Handle the initial sign-in state.
        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        authorizeButton.onclick = handleAuthClick;
        signoutButton.onclick = handleSignoutClick;
      });
  };

  /**
   *  On load, called to load the auth2 library and API client library.
   */
  const handleClientLoad = () => gapi.load('client:auth2', initClient);

  dateChangeListeners(gapi);
  refreshButtonListener(gapi);
  pointLayerListener(gapi);
  handleClientLoad();
});
