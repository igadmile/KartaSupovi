import { createLastPointLayer } from '../map-point';
import data from '../../../data/test-data';

test('Last point instance defined and data added', () => {
  const pointLayer = createLastPointLayer();
  pointLayer.addData(data);

  const layersKeys = Object.keys(pointLayer._layers);
  const geometryType = pointLayer._layers[layersKeys[0]].feature.geometry.type;

  expect(geometryType).toBe('Point');
});
