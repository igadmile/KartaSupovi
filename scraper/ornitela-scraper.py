from datetime import datetime, timedelta
from selenium.webdriver import Firefox
from selenium.webdriver import FirefoxProfile
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from sys import argv
import json
import time
import os

def firefox_profile(dl_location):
    profile = FirefoxProfile()
    profile.set_preference("browser.download.folderList", 2)
    profile.set_preference("browser.download.manager.showWhenStarting", False)
    profile.set_preference("browser.download.dir", dl_location)
    profile.set_preference(
        "browser.helperApps.neverAsk.saveToDisk", "application/octet-stream kml gpx")

    return(profile)


def login(u, p):
    username = driver.find_element_by_name('username')
    password = driver.find_element_by_name('password')
    username.send_keys(u)
    password.send_keys(p)
    username.send_keys(Keys.RETURN)


def open_device(selector):
    element = driver.find_element_by_css_selector(selector)
    element.click()


def date_range(startdate):
    start_date = driver.find_element_by_id('idfromdt')
    start_date.clear()
    start_date.send_keys(startdate)

def date_diff(datetime, diff):
    now = datetime.today()
    date_diff = now - timedelta(diff)
    return ("%d-%d-%dT%d:%d" % (date_diff.year, date_diff.month, date_diff.day, date_diff.hour, date_diff.minute))


def check_download(extension, path):
    if any(File.endswith(extension) for File in os.listdir(path)):
        time.sleep(2)
        check_download(extension, path)


def rename_files(name, devices, url):
    for subdir, dirs, files in os.walk(url):
        for file in files:
            if file.endswith('.kml'):
                if file[0:6] == devices[name]:
                    os.rename('%s/%s' % (url, file), '%s/%s.kml' % (url, name))


def remove_old_kml(url):
    for subdir, dirs, files in os.walk(url):
        for file in files:
            if file.endswith('.kml'):
                os.remove('%s/%s' % (url, file))


if __name__ == "__main__":
    arguments = {'dl_location' : argv[1], 'birds' : json.loads(argv[2]), 'start_date': argv[3], 'username': argv[4], 'password': argv[5]}

    if not os.path.exists('./KML'):
        os.makedirs('./KML')
    options = Options()
    options.add_argument('-headless')
    driver = Firefox(executable_path='geckodriver',
                     firefox_options=options, firefox_profile=firefox_profile(arguments['dl_location']))

    wait = WebDriverWait(driver, 10)
    driver.get('https://www.glosendas.net/users/')
    login(arguments['username'], arguments['password'])
    element = wait.until(EC.element_to_be_clickable(
        (By.ID, 'devdatatbl_length')))

    devices = arguments['birds']
    remove_old_kml('./KML')
    for bird in devices:
        open_device('th[data-order="%s"]' % devices[bird])
        time.sleep(5)
        date_range(arguments['start_date'])
        open_device('input[title="Download KML file"]')
        check_download('.part', './KML')
        rename_files(bird, devices, './KML')

    driver.quit()
