const checkSmallScreen = () => window.innerWidth < 1200;

export default checkSmallScreen;
