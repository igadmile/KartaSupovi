import generateDate from '../generate-date';

const testDate = new Date(2018, 5, 1, 5, 5);
test('Generate end date', () => {
  expect(generateDate(testDate)).toBe('2018-06-01T05:05');
});
