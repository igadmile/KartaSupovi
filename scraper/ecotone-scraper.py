import requests
import zipfile
import datetime
import time
import os
from sys import argv
import json
from dateutil.rrule import rrule, MONTHLY


def download_file(url, user, password):
    local_filename = url.split('/')[-1]
    r = requests.get(url, stream=True, auth=(user, password))
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    return local_filename


def rename_files(name, devices, url):
    for subdir, dirs, files in os.walk(url):
        for file in files:
            print(file[10:28])
            if file.endswith('.kml'):
                if file[10:28] == devices[name]:
                    os.rename('%s/%s' % (url, file), '%s/%s.kml' % (url, name))


start_date = datetime.date(int(argv[2]), int(argv[3]), int(argv[4]))
now_date = datetime.datetime.now()
tracker_ids = json.loads(argv[1])


for dt in rrule(MONTHLY, dtstart=start_date, until=now_date):
    for tracker in tracker_ids:
        if not os.path.exists('./KML/%s' % tracker):
            os.makedirs('./KML/%s' % tracker)

        url = "http://telemetry.ecotone.pl/crovultures/paths_positions/month/{a}{b}/POSITIONS_{c}_{a}{b}.kmz".format(
            a=dt.strftime("%Y"), b=dt.strftime("%m"), c=tracker_ids[tracker])
        path = download_file(url, argv[5], argv[6])
        zip_ref = zipfile.ZipFile(path, 'r')
        zip_ref.extractall(".")
        zip_ref.close()
        os.rename('./POSITIONS_{c}_{a}{b}.kml'.format(
            a=dt.strftime("%Y"), b=dt.strftime("%m"), c=tracker_ids[tracker]), './KML/{d}/POSITIONS_{c}_{a}{b}.kml'.format(
                a=dt.strftime("%Y"), b=dt.strftime("%m"), c=tracker_ids[tracker], d=tracker))
