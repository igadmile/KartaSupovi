#!/bin/bash
path=.

# Make sure we don't iterate over spaces in layer names
IFS=$'\n'
for i in KML/*.kml; do
    layers="$(ogrinfo -q -ro -so "$i" | perl -pe 's/^[^ ]+ //g and s/ \([^()]+\)$//g')"
    for layer in $layers; do
        if [[ $layer = *"points"* ]]; then
            echo $layer
            #IFS=$SAVEIFS
            filename=$(basename "$i")
            filename="${filename%.*}"
            echo "File: $filename, layer $layer"
            ogr2ogr -f "GeoJSON" -lco COORDINATE_PRECISION=5 "$path/${filename}.json" "$i" "${layer}"
            echo "$path/${filename}.json"
            sed -i -e 's/\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s\s//g' "$path/${filename}.json"
            sed -i -e 's/:\s/:/g' "$path/${filename}.json"
            sed -i -e 's/{\s/{/g' "$path/${filename}.json"
            sed -i -e 's/\s}/}/g' "$path/${filename}.json"
            sed -i -e 's/\[\s/\[/g' "$path/${filename}.json"
            sed -i -e 's/\s\]/\]/g' "$path/${filename}.json"
            sed -i -e 's/\,\s/\,/g' "$path/${filename}.json"
            sed -i -e 's/\\n//g' "$path/${filename}.json"
            sed -i -e 's/\,[0-9]*\.[0-9]\]/\]/g' "$path/${filename}.json"
            sed -i -e 's/\\//g' "$path/${filename}.json"
            sed -i -e 's/<table>\s<tr/<table><tr/g' "$path/${filename}.json"
            sed -i -e 's/tr>\s<tr/tr><tr/g' "$path/${filename}.json"
            sed -i -e 's/\sUTC\"/"/g' "$path/${filename}.json"
            sed -i -e 's/^.*0\.0\,0\.0.*//g' "$path/${filename}.json"
            sed -i -e '/^\s*$/d' "$path/${filename}.json"
            sed -i -e 's/<tr><th>GPS fix time<\/th><td>[^/]*<\/td><\/tr>//g' "$path/${filename}.json"
            sed -i -e 's/<tr><th>&nbsp;<\/th><td>&nbsp;<\/td><\/tr>//g' "$path/${filename}.json"
            sed -i -e 's/<tr><th>HDOP<\/th><td>[^/]*<\/td><\/tr>\s//g' "$path/${filename}.json"
        fi
    done
    #IFS=$SAVEIFS
done
rm KML/*.kml
