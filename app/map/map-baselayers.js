import L from 'leaflet';

const baseLayers = {
  'Thunderforest-Outdoors': L.tileLayer(
    'https://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png?apikey=f13946a5090e4a73aded2506761ae4eb',
    {
      attribution: `Kartu izradila <a href="http://www.biom.hr/">Udruga BIOM</a>
        . Podloge - <a href="https://www.thunderforest.com">Thunderforest</a>`,
    },
  ),
  'ESRI-Satellite': L.tileLayer(
    'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
    {
      attribution: `Kartu izradila <a href="http://www.biom.hr/">Udruga BIOM</a>
        . Podloge - <a href="https://www.esri.com/en-us/home">ESRI</a>`,
    },
  ),
  'Carto-Dark': L.tileLayer(
    'https://cartodb-basemaps-c.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png',
    {
      attribution: `Kartu izradila <a href="http://www.biom.hr/">Udruga BIOM</a>
        . Podloge - <a href="https://carto.com/">Carto</a>`,
    },
  ),
};

export default baseLayers;
