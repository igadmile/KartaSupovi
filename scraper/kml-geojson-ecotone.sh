#!/bin/sh

for directory in ./KML/* ; do
    if [ -d "$directory" ]; then
        for kml in "$directory"/*.kml; do
            echo "$kml"
            ogr2ogr -f GeoJSON -select name "${kml%.*}.json" "$kml" -nln ${directory#*KML/} -append
        done
        
        for json in "$directory"/*.json; do
            sed -i -e 's/\,\s[0-9]\.[0-9]\s\]/\]/g' "${json}"
            sed -i -e 's/\"CROV[0-9]*\-[0-9]*\s/\"/g' "${json}"
            sed -i -e 's/\s\[[0-9]*\]"/\"/g' "${json}"
            sed -i -e 's/{\s\"type\"\:\s\"Feature\"\,\s\"properties\"\:\s{\s\"Name\"\:\s\"\${DESCR}\".*//g' "${json}"
            sed -i -e 's/{\s\"type\"\:\s\"Feature\"\,\s\"properties\"\:\s{\s\"Name\"\:\s\"START\".*//g' "${json}"
            sed -i -e 's/{\s\"type\"\:\s\"Feature\"\,\s\"properties\"\:\s{\s\"Name\"\:\s\"END\".*//g' "${json}"
            sed -i -e 's/}*$//g' "${json}"
            sed -i -e 's/\]*$//g' "${json}"
            sed -i -e '/^\s*$/d'  "${json}"
            sed -i -e '$s/\s}\s}\,/}}/g'  "${json}"
            sed -i -e "\$a\]}"  "${json}"
        done
        
        python merge-json.py -p 5 "$directory"/*.json ./${directory#*KML/}.json
        rm "$directory"/*
    fi
done

rm ./*.kmz
