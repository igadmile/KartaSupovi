const data = {
  type: 'FeatureCollection',
  crs: {
    type: 'name',
    properties: { name: 'urn:ogc:def:crs:OGC:1.3:CRS84' }
  },
  features: [
    {
      type: 'Feature',
      properties: {
        Name: '2018-05-04 00:05:10',
        Description:
          "<table><tr><th>Date/Time</th><td>2018-05-04 02:05:10 (UTC+2)</td></tr><tr><th>Battery</th><td>77% (4013 mV)</td></tr><tr><th>Solar I</th><td>0 mA</td></tr><tr><th>Light</th><td>0</td></tr><tr><th>Temperature</th><td>23 'C</td></tr><tr><th>Accelerometer</th><td>X=-6 Y=-12 Z=1034</td></tr><tr><th>&nbsp;</th><td>&nbsp;</td></tr><tr><th>GPS fix time</th><td>20160 ms</td></tr><tr><th>Longitude</th><td>14.351495</td></tr><tr><th>Latitude</th><td>45.114716</td></tr><tr><th>Speed</th><td>0 km/h</td></tr><tr><th>Altitude</th><td>96 meters</td></tr><tr><th>HDOP</th><td>1.7</td></tr> </table>"
      },
      geometry: { type: 'Point', coordinates: [14.35149, 45.11472] }
    },
    {
      type: 'Feature',
      properties: {
        Name: '2018-05-04 00:15:09',
        Description:
          "<table><tr><th>Date/Time</th><td>2018-05-04 02:15:09 (UTC+2)</td></tr><tr><th>Battery</th><td>77% (4013 mV)</td></tr><tr><th>Solar I</th><td>0 mA</td></tr><tr><th>Light</th><td>0</td></tr><tr><th>Temperature</th><td>22 'C</td></tr><tr><th>Accelerometer</th><td>X=-6 Y=-14 Z=1033</td></tr><tr><th>&nbsp;</th><td>&nbsp;</td></tr><tr><th>GPS fix time</th><td>18630 ms</td></tr><tr><th>Longitude</th><td>14.351145</td></tr><tr><th>Latitude</th><td>45.11451</td></tr><tr><th>Speed</th><td>2 km/h</td></tr><tr><th>Altitude</th><td>117 meters</td></tr><tr><th>HDOP</th><td>1.5</td></tr> </table>"
      },
      geometry: { type: 'Point', coordinates: [14.35115, 45.11451] }
    },
    {
      type: 'Feature',
      properties: {
        Name: '2018-05-04 00:25:12',
        Description:
          "<table><tr><th>Date/Time</th><td>2018-05-04 02:25:12 (UTC+2)</td></tr><tr><th>Battery</th><td>77% (4013 mV)</td></tr><tr><th>Solar I</th><td>0 mA</td></tr><tr><th>Light</th><td>0</td></tr><tr><th>Temperature</th><td>22 'C</td></tr><tr><th>Accelerometer</th><td>X=-5 Y=-14 Z=1038</td></tr><tr><th>&nbsp;</th><td>&nbsp;</td></tr><tr><th>GPS fix time</th><td>21650 ms</td></tr><tr><th>Longitude</th><td>14.35085</td></tr><tr><th>Latitude</th><td>45.114265</td></tr><tr><th>Speed</th><td>0 km/h</td></tr><tr><th>Altitude</th><td>152 meters</td></tr><tr><th>HDOP</th><td>1.5</td></tr> </table>"
      },
      geometry: { type: 'Point', coordinates: [14.35085, 45.11427] }
    }
  ]
};

export default data;
