#!/bin/bash
HOST=''
USER=''
PASSWD=''

# needed becouse of crontab
cd /PATH/TO/SCRAPER/DIRECTORY/

python ecotone-scraper.py JSON-object-animals start-year start-month start-date username password
./kml-geojson-ecotone.sh
ftp -n -v $HOST << EOT
ascii
user $USER $PASSWD
prompt
cd DIRECTORY/ON/SERVER
mput ./*.json
# cd ..
# put index.html
bye
EOT

rm ./*.json
