const addLeadingZeros = datetime => (datetime.length < 2 ? `0${datetime}` : datetime);

const generateDate = (nowDate) => {
  const year = nowDate.getFullYear().toString();
  const month = addLeadingZeros((nowDate.getMonth() + 1).toString());
  const day = addLeadingZeros(nowDate.getDate().toString());
  const hours = addLeadingZeros(nowDate.getHours().toString());
  const minutes = addLeadingZeros(nowDate.getMinutes().toString());

  return `${year}-${month}-${day}T${hours}:${minutes}`;
};

export default generateDate;
