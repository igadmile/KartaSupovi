import { createLineLayer, genLineData } from '../map-line';
import data from '../../../data/test-data';

test('Line class', () => {
  const lineLayer = createLineLayer('jellow');
  lineLayer.addData(genLineData(data));

  const layersKeys = Object.keys(lineLayer._layers);
  const geometryType = lineLayer._layers[layersKeys[0]].feature.geometry.type;

  expect(geometryType).toBe('LineString');
});
