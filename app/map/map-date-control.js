import L from 'leaflet';

const dateControlHTML = `<div id="calendar-toggle" class="glyphicon glyphicon-calendar" aria-hidden="true"></div>
<form id="calendar">
<label style="margin-bottom: 10px;">
  <input id="show-points" type="checkbox"> Prikaži točke kretanja
</label>
<label for="date-start">Prikaži zadnjih</label>
<div class="btn-group form-group" role="group" aria-label="...">
  <button id="two" type="button" class="btn btn-default">2 dana</button>
  <button id="seven" type="button" class="btn btn-default">7 dana</button>
  <button id="thirty" type="button" class="btn btn-default">30 dana</button>
  <button id="all" type="button" class="btn btn-default">Cijeli period</button>
</div>
<div class="form-group">
    <label for="date-start">Odaberite početni datum</label>
    <input type="datetime-local" class="form-control" id="date-start">
</div>
<div class="form-group">
    <label for="date-end">Odaberite završni datum</label>
    <input type="datetime-local" class="form-control" id="date-end">
</div>
</form>`;

const DateControl = L.Control.extend({
  onAdd: (map) => {
    const mapWidth = map.getSize().x;
    const container = L.DomUtil.create('div', 'leaflet-control-layers leaflet-control signin');
    container.id = 'date-control';
    container.style.minWidth = '44px';
    container.innerHTML = dateControlHTML;

    if (mapWidth < 1200) {
      container.querySelector('#calendar').style.display = 'none';
    } else {
      container.querySelector('#calendar-toggle').style.display = 'none';
    }

    container.querySelector('#calendar-toggle').onclick = () => {
      if (container.querySelector('#calendar').style.display === 'none') {
        container.querySelector('#calendar').style.display = 'block';
      } else if (container.querySelector('#calendar').style.display === 'block') {
        container.querySelector('#calendar').style.display = 'none';
      }
    };

    L.DomEvent.disableClickPropagation(container);

    return container;
  },
});

export default DateControl;
