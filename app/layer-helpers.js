import L from 'leaflet';
import {
  animals,
  pointLayers,
  lastPoint,
  layerControl,
  layerCluster,
  lineLayers,
  map,
} from './map/map';
import { createPointLayer, createLastPointLayer } from './map/map-point';
import { createLineLayer, genLineData } from './map/map-line';
import 'regenerator-runtime/runtime';
import checkSmallScreen from './util/check-small-screen';

const createLayers = () => {
  const colors = [
    'rgb(255,186,39)',
    'rgb(42,186,39)',
    'rgb(180,100,120)',
    'rgb(50,100,155)',
    'rgb(15,15,15)',
    'rgb(227,26,28)',
    'rgb(255,42,195)',
    'rgb(6,195,192)',
    'rgb(173,30,212)',
  ];

  animals.forEach((animal, index) => {
    lineLayers[animal] = createLineLayer(colors[index]);
    pointLayers[animal] = createPointLayer(colors[index]);
    lastPoint[animal] = createLastPointLayer();
  });
};

const clearLayers = () => {
  animals.forEach((animal) => {
    layerCluster[animal].clearLayers();
    pointLayers[animal].clearLayers();
    lastPoint[animal].clearLayers();
    lineLayers[animal].clearLayers();
  });
};

const addLayerControl = () => {
  layerControl.addTo(map);
  if (checkSmallScreen()) {
    layerControl.collapse();
  }
};

const filterData = (feature) => {
  const minDate = new Date(document.getElementById('date-start').value);
  const maxDate = new Date(document.getElementById('date-end').value);
  const featureDate = new Date(feature.properties.Name);
  return feature && featureDate <= maxDate && featureDate >= minDate;
};

const fetchData = async (urlServer, animal) => {
  const minDate = new Date(document.getElementById('date-start').value);
  const maxDate = new Date(document.getElementById('date-end').value);
  const fileName = maxDate - minDate <= 2595600000
    ? `${urlServer}data/${animal}_30.json`
    : `${urlServer}data/${animal}.json`;

  const response = await fetch(fileName);
  const data = await response.json();
  data.features = data.features.filter(filterData);
  return data;
};

const addData = (data, animal) => {
  // if there is no data, skip the layer and do not add it to the map
  if (data.features.length > 0) {
    const showPointLayers = document.getElementById('show-points').checked;

    lineLayers[animal].addData(genLineData(data));
    lastPoint[animal].addData(data.features[data.features.length - 1]);

    layerCluster[animal].addLayer(lineLayers[animal]);
    layerCluster[animal].addLayer(lastPoint[animal]);

    if (showPointLayers) {
      pointLayers[animal].addData(data);
      layerCluster[animal].addLayer(pointLayers[animal]);
    }

    map.addLayer(layerCluster[animal]);
  } else {
    map.removeLayer(layerCluster[animal]);
  }
};

const getLayerBounds = () => {
  const bounds = L.latLngBounds([]);

  animals.forEach((animal) => {
    bounds.extend(layerCluster[animal].getBounds());
  });
  return bounds;
};

const addLayersFirstLoad = () => {
  const urlServer = 'REPLACE_STRING_urlServer';

  animals.forEach(async (animal, index) => {
    const data = await fetchData(urlServer, animal);
    addData(data, animal);

    if (index === animals.length - 1) {
      map.fitBounds(getLayerBounds());
    }
  });
};

const addLayersAfterLoad = () => {
  const urlServer = 'REPLACE_STRING_urlServer';

  animals.forEach(async (animal) => {
    const data = await fetchData(urlServer, animal);
    addData(data, animal);
  });
};

const removeLayers = () => {
  map.removeControl(layerControl);
  clearLayers();

  animals.forEach((animal) => {
    map.removeLayer(layerCluster[animal]);
  });
};

export {
  createLayers,
  addLayersFirstLoad,
  addLayersAfterLoad,
  clearLayers,
  removeLayers,
  addLayerControl,
};
