import generateDate from './generate-date';
import checkSmallScreen from '../util/check-small-screen';
import { afterLoad } from '../main';

const startDate = 'REPLACE_STRING_dateTime';
const startDateElement = document.getElementById('date-start');
const endDateElement = document.getElementById('date-end');
const dateButtons = {
  twoDays: document.getElementById('two'),
  sevenDays: document.getElementById('seven'),
  thirtyDays: document.getElementById('thirty'),
  allDays: document.getElementById('all'),
};

const dateDiff = (element, days) => {
  // copy element to avoid parameter reassign
  const elementCopy = element;
  const date = new Date();
  date.setDate(date.getDate() - days);

  elementCopy.value = generateDate(date);
  element.dispatchEvent(new Event('change'));
};

const dateSwitch = (element, diff) => {
  dateDiff(startDateElement, diff);
  element.classList.add('active');
};

const removeActiveClass = (element) => {
  Object.values(dateButtons).forEach((button) => {
    if (button !== element) {
      button.classList.remove('active');
    }
  });
};

dateButtons.twoDays.onclick = () => {
  dateSwitch(dateButtons.twoDays, 2);
  removeActiveClass(dateButtons.twoDays);
};
dateButtons.sevenDays.onclick = () => {
  dateSwitch(dateButtons.sevenDays, 7);
  removeActiveClass(dateButtons.sevenDays);
};
dateButtons.thirtyDays.onclick = () => {
  dateSwitch(dateButtons.thirtyDays, 30);
  removeActiveClass(dateButtons.thirtyDays);
};

dateButtons.allDays.onclick = () => {
  startDateElement.value = startDate;
  startDateElement.dispatchEvent(new Event('change'));
  dateButtons.allDays.classList.add('active');
  removeActiveClass(dateButtons.allDays);
};

const dateChangeListeners = (gapi) => {
  document.getElementById('date-start').onchange = () => afterLoad(gapi);
  document.getElementById('date-end').onchange = () => afterLoad(gapi);
};

if (checkSmallScreen()) {
  dateSwitch(dateButtons.sevenDays, 7);
} else {
  dateSwitch(dateButtons.thirtyDays, 30);
}

endDateElement.value = generateDate(new Date());

export default dateChangeListeners;
