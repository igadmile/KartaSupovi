import { afterLoad } from '../main';

const refreshButtonListener = (gapi) => {
  document.getElementById('refresh').onclick = () => afterLoad(gapi);
};

export default refreshButtonListener;
