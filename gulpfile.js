const gulp = require('gulp');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const uglify = require('gulp-uglify');
const replace = require('gulp-replace');
const { argv } = require('yargs');

gulp.task('default', ['transpile']);

gulp.task('transpile', () => {
  gulp
    .src(['index.html'])
    .pipe(replace('PAGENAME', argv.pagename))
    .pipe(replace('VERSION', Math.floor(Math.random() * 1000 + 1).toString()))
    .pipe(gulp.dest('dist/'));

  gulp.src(['css/style.css']).pipe(gulp.dest('dist/'));
  gulp.src('data/**.*').pipe(gulp.dest('dist/data'));

  return browserify('app/auth.js')
    .transform('babelify', {
      presets: ['@babel/preset-env'],
    })
    .bundle()
    .on('error', (error) => {
      console.error('\nError: ', error.message, '\n');
      this.emit('end');
    })
    .pipe(source('bundle.js'))
    .pipe(replace('REPLACE_STRING_CLIENT_ID', argv.client))
    .pipe(replace('REPLACE_STRING_API_KEY', argv.api))
    .pipe(replace('REPLACE_STRING_spreadsheetId', argv.spreadsheet))
    .pipe(replace('REPLACE_STRING_range', argv.range))
    .pipe(replace('REPLACE_STRING_dateTime', argv.datetime))
    .pipe(replace('REPLACE_STRING_urlServer', argv.production))
    .pipe(replace("'REPLACE_STRING_animals'", argv.animals))
    .pipe(replace('REPLACE_STRING_sponsor', argv.sponsor))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('dist'));
});

gulp.task('watch', ['transpile'], () => {
  gulp.watch('./**/*.js', ['transpile']);
});
