import L from 'leaflet';

const mainControlHTML = `REPLACE_STRING_sponsor
<form class="form">
<button id="authorize-button" style="display: none;float:left;margin-right:10px;" type="button" class="btn btn-success">Authorize</button>
<button id="signout-button" style="display:none;float:left;margin-right:10px;" type="button" class="btn btn-warning">Sign Out</button>
<button type="button" id="refresh" class="btn btn-primary">Osvježi kartu</button>
</form>
<div id="content" class="alert alert-danger" style="display:none;float:left;" role="alert"></div>`;

const MainControl = L.Control.extend({
  onAdd: (map) => {
    const mapWidth = map.getSize().x;
    const container = L.DomUtil.create('div', 'leaflet-control-layers leaflet-control signin');

    container.style.minWidth = '213px';
    container.innerHTML = mainControlHTML;

    if (mapWidth < 1200) {
      container.style.marginBottom = '24px';
    }

    L.DomEvent.disableClickPropagation(container);

    return container;
  },
});

export default MainControl;
