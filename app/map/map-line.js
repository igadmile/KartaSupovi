import L from 'leaflet';

const onEachFeatureLine = (feature, layer) => {
  const distance = layer._latlngs.reduce((dist, latnlngs, index, array) => {
    const i = index === 0 ? 1 : index;
    return dist + array[i - 1].distanceTo(latnlngs);
  }, 0);

  const popupContent = `<div style="text-align: center; margin-left: auto; margin-right: auto;font-size:14px;">
  <strong>Udaljenost: ${(distance / 1000).toFixed(2)} km</strong>
  </div>`;
  layer.bindPopup(popupContent);
};

const createLineLayer = color => new L.geoJson(null, {
  style: {
    opacity: 1,
    color,
    weight: 3.0,
  },
  onEachFeature: onEachFeatureLine,
});

const genLineData = (data) => {
  const { features } = data;
  const c = {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        geometry: {
          type: 'LineString',
          coordinates: [],
        },
      },
    ],
  };

  Object.values(features).forEach((value) => {
    c.features[0].geometry.coordinates.push(value.geometry.coordinates);
  });
  return c;
};

export { createLineLayer, genLineData };
