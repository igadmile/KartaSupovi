#!/bin/bash

if [ "$1" == "local" ]; then
    production="http://localhost:8000/"
else
    production="http://supovi.biom.hr/"
fi

echo "$production"

gulp --pagename 'Supovi - Udruga Biom' \
--client '838819826587-eohslf2recopp325kabov187th8gra0f.apps.googleusercontent.com' \
--api 'AIzaSyAv14-BBFdjpCCQ6AzKKzxIESw29cHIhu0' \
--spreadsheet '1B1GSL6WdZHvAZgiDDzmeYFEYCSt3p1l2oYgjsHvejaA' \
--range 'main!A:J' \
--datetime '2018-05-04T00:00' \
--production "$production" \
--animals "'Jadran', 'Kvarneric', 'Pavlomir', 'Nevera', 'Kruna', 'Tramuntana', 'Kvarner', 'Perun', 'Kupala'" \
--sponsor "<img style='margin-bottom:10px;' src='interreg.png'>"