Interactive map for tracking the movement of birds tagged with the [Ecotone](http://www.ecotone.pl/index.php/en/produkty/telemetry) and [OrniTrack](https://www.ornitela.com/ornitrack) GPS GSM devices.

The user has to log-in with the Google account and has to have permission to access the map.

The data from the manufacturer system is scraped and uploaded to the server using the python and bash scripts.

# Technologies
## Map
- leafletjs
- google-client-api

## Scraper
- Beautiful Soup
- Selenium Python (Geckodriver and Firefox)
- bash
- gdal

# Feature overview
![overview](https://gitlab.com/igadmile/KartaSupovi/raw/d27652c0bb3f2fbb1765d3ffe29d307497b22245/overview_1.jpg)

## Map

The map displays the tracks of animals for the chosen time period. By default, the tracks for the last 30 days are displayed.

Clicking on the track opens the popup with the number of kilometers that the animal has moved during the selected period.

## Login control (bottom left)
- Buttons for log-in/ log-out
- Button "Osvježi kartu" when clicked re-fetches data from the server

## Date control (bottom right)
The user can filter the data based by choosing the:
- Start date ("Odaberite početni datum").
- End date ("Odaberite završni datum").
- Automatically filter the data by two days ("2 dana" button), seven days ("7 dana") button or thirty days ("30 dana") button.

Additionally, the user can toggle the "Prikaži točke kretanja" checkbox to show/hide points for every GPS fix.
![overview points](https://gitlab.com/igadmile/KartaSupovi/raw/d27652c0bb3f2fbb1765d3ffe29d307497b22245/overview_2.jpg)

Clicking on the point opens the popup with the additional information.

## Scraper
- The scraper for the Ecotone system is written with the Beautiful Soup python library.
- The scraper for the OrniTrax system is written with the Selenium python library because the site uses ajax requests.

When the data is downloaded (as KML files) it is converted to the GeoJson using the ogr2ogr from the gdal package.

The data is updated to the server using the ftp protocol.

There is the bash script written for every system (update-server-ecotone.sh, update-server-ornitela.sh) which automates the process.

# Running the tests
`npm test` or `jest` (jest has to be installed).

`jest --watch` for running the tests in watch mode.
